"use strict";

const express = require("express");
const appConfig = require("config").get("app");
const service = require("./services/userService");
const logger = require("@open-age/logger")("server");
const fs = require('fs');
// const Http = require("http"); for http req
const https = require('https');
const port = process.env.PORT || appConfig.port || 3000;
var admin = require("firebase-admin");
var serviceAccount = require("./firebase-truckapp.json");
const app = express();

var bodyParser = require('body-parser');

const options = {
  cert: fs.readFileSync('/etc/letsencrypt/live/surveyapp.cradle.services/fullchain.pem'),
  key: fs.readFileSync('/etc/letsencrypt/live/surveyapp.cradle.services/privkey.pem')
};

var server = https.createServer(options, app);
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const boot = () => {
  const log = logger.start("app:boot");
  log.info(`environment:  ${process.env.NODE_ENV}`);
  log.info("starting server");
  server.listen(port, () => {
    log.info(`listening on port: ${port}`);
    log.end();
  });
};

const init = async () => {
  await require("./settings/database").configure(logger);
  await require("./settings/express").configure(app, logger);
  await require("./settings/routes").configure(app, logger);
  boot();
};

init();


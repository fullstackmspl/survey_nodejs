module.exports = [{
    url: "/create",
    post: {
        summary: "create",
        description: "create",
        parameters: [{
            in: "body",
            name: "body",
            description: "Model of user creation",
            required: true,
            schema: {
                $ref: "#/definitions/userCreate"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/login",
    post: {
        summary: "login",
        description: "login",
        parameters: [{
            in: "body",
            name: "body",
            description: "Model of user login",
            required: true,
            schema: {
                $ref: "#/definitions/login"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/resetPassword",
    post: {
        summary: "Reset Password",
        description: "reset Password",
        parameters: [{
            in: "header",
            name: "x-access-token",
            description: "token to access api",
            required: true,
            type: "string"
        },
        {
            in: "body",
            name: "body",
            description: "Model of resetPassword user",
            required: true,
            schema: {
                $ref: "#/definitions/resetPassword"
            }
        }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/getUsers",
    get: {
        summary: "getUsers",
        description: "Just hit the api without pass any param",
        parameters: [{
            name: "API Token",

            description: "No Need To Pass Any Param ",
            schema: {
                $ref: "#/definitions/getUsers"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/currentUser",
    post: {
        summary: "currentUser",
        description: "currentUser",
        parameters: [{
            in: "body",
            name: "body",
            description: "Model of Current User",
            required: true,
            schema: {
                $ref: "#/definitions/currentUser"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/delete",
    post: {
        summary: "delete",
        description: "delete",
        parameters: [{
            in: "body",
            name: "body",
            description: "Model of user login",
            required: true,
            schema: {
                $ref: "#/definitions/deleteUser"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/update",
    post: {
        summary: "update",
        description: "update",
        parameters: [{
            in: "body",
            name: "body",
            description: "Model of user login",
            required: true,
            schema: {
                $ref: "#/definitions/updateUser"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/uploadProfilePic/{id}",
    put: {
        summary: "upload Profile Pic ",
        description: "upload Profile Pic ",
        parameters: [{
            in: "formData",
            name: "image",
            type: "file",
            description: "The file to upload.",
            required: true,
        },
        {
            in: "path",
            type: "string",
            name: "id",
            description: "user id",
            required: true
        },
            // { in: "header",
            //     name: "x-access-token",
            //     description: "token to access api",
            //     required: true,
            //     type: "string"
            // }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

{
    url: "/getProvidersByDate",
    get: {
        summary: "get Provider's list date wise",
        description: "get Provider's list date wise",
        parameters: [
            {
                in: "query",
                name: "fromDate",
                description: "Date belongs From date",
                type: "string"
            },
            {
                in: "query",
                name: "toDate",
                description: "Date belongs To date",
                type: "string"
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

{
    url: "/addDetails",
    post: {
        summary: "addDetails",
        description: "addDetails",
        parameters: [{
            in: "body",
            name: "body",
            description: "Model of detail creation",
            required: true,
            schema: {
                $ref: "#/definitions/addDetail"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
];
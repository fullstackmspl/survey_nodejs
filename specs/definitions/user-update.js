module.exports = [{
    name: "updateUser",
    properties: {
        _id: {
            type: "string"
        },
        firnastName: {
            type: "string"
        },
        lastName: {
            type: "string"
        },
        phoneNumber: {
            type: 'string'
        },
        sex: {
            type: 'string'
        },
        role: {
            type: "string",
            default: "parent",
            enum: ["provider", "child", "admin"]
        },
    }
}];
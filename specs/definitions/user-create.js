module.exports = [

    {
        name: "userCreate",
        properties: {

            name: {
                type: "string"
            },
            lastName: {
                type: "string"
            },
            email: {
                type: "string"
            },

            password: {
                type: "string"
            },

            role: {
                type: "string"
            },
        }
    }
];
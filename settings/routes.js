"use strict";

const fs = require("fs");
const api = require("../api");
const specs = require("../specs");
const permit = require("../permit")
const path = require("path");
const validator = require("../validators");
var multer = require('multer');

var storage = multer.diskStorage({

    destination: function(req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, path.join(__dirname, '../', 'assets'));
        } else {
            cb(null, path.join(__dirname, '../', 'assets/images'));
        }
    },
    filename: function(req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, file.originalname);
        } else {
            cb(null, Date.now() + file.originalname);
        }
    }
});

var upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });

const configure = (app, logger) => {
    const log = logger.start("settings:routes:configure");
    app.get("/specs", function(req, res) {
        fs.readFile("./public/specs.html", function(err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                });
            }
            res.contentType("text/html");
            res.send(data);
        });
    });
    app.get("/api/specs", function(req, res) {
        res.contentType("application/json");
        res.send(specs.get());
    });

    app.post(
        "/api/users/create",
        permit.context.builder,
        validator.users.create,
        api.users.create
    );

    // app.post(
    //     "/api/admin/create",
    //     permit.context.builder,
    //     validator.admin.create,
    //     api.admin.create
    // );

    app.post(
        "/api/users/login",
        permit.context.builder,
        validator.users.login,
        api.users.login
    );

    app.post(
        "/api/users/currentUser",
        permit.context.builder,
        api.users.currentUser
    );

    app.post(
        "/api/users/changePassword",
        permit.context.builder,
        api.users.changePassword,
        validator.users.changePassword,

    );

    app.post(
        "/api/users/addDetails",
        permit.context.builder,
        api.users.addDetails,
        // validator.users.addDetails,
    );

    app.post(
        "/api/users/tellAFriend",
        permit.context.builder,
        api.users.tellAFriend
      );

      app.post(
        "/api/users/sendMessage",
        permit.context.builder,
        api.users.sendMessage
      );
      
    app.post(
        "/api/users/updateCustomer",
        permit.context.builder,
        validator.users.updateCustomer,
        api.users.updateCustomer
    );

    app.post(
        "/api/users/addOffers",
        permit.context.builder,
        api.users.addOffers,
        validator.users.addOffers,
    );

    app.post(
        "/api/users/addGasType",
        permit.context.builder,
        api.users.addGasType,
        // validator.users.addOffers,
    );

    app.post(
        "/api/users/voucher",
        permit.context.builder,
        api.users.voucher,
        // validator.users.addOffers,
    );


    // app.get(
    //     "/api/users/getLocation",
    //     permit.context.builder,
    //     api.users.getLocation,
    //     // validator.users.getOffers,
    // );

    app.get(
        "/api/users/getDetails",
        permit.context.builder,
        api.users.getDetails,
        // validator.users.getOffers,
    );

    app.get(
        "/api/users/getOffers",
        permit.context.builder,
        api.users.getOffers,
        // validator.users.getOffers,
    );

    app.post(
        "/api/users/voucherMail",
        permit.context.builder,
        api.users.voucherMail
    );

    app.get(
        "/api/users/getUsers",
        permit.context.builder,
        api.users.getUsers
    );

    app.get(
        "/api/users/getGasType",
        permit.context.builder,
        api.users.getGasType
    );

    // app.post(
    //     "/api/users/usersByDate",
    //     permit.context.builder,
    //     api.users.usersByDate
    // );
    
    app.post(
        "/api/users/deleteGasType",
        permit.context.builder,
        api.users.deleteGasType
    );

    app.post(
        "/api/users/deleteOffers",
        permit.context.builder,
        api.users.deleteOffers
    );
    

    app.post(
        "/api/users/delete",
        permit.context.builder,
        api.users.deleteUser
    );

    app.post(
        "/api/users/update",
        permit.context.builder,
        validator.users.update,
        api.users.update
    );

    app.put(
        "/api/users/uploadProfilePic/:id",
        permit.context.builder,
        upload.single('image'),
        api.users.uploadProfilePic
    );


    app.get(
        "/api/users/getProvidersByDate",
        permit.context.builder,
        // validator.users.get, 
        api.users.getProvidersByDate
      );

    log.end();
};

exports.configure = configure;
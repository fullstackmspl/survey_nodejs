"use strict";

exports.toModel = entity => {
    const model = {
        _id: entity._id,
        name: entity.name,
        phoneNumber: entity.phoneNumber,
        email: entity.email,
        isOnBoardingDone: entity.isOnBoardingDone,
        address: entity.address,
        city: entity.city,
        country: entity.country,
        zipCode: entity.zipCode,
        lat: entity.lat,
        lng: entity.lng,
        stripeToken: entity.stripeToken,
        stripeKey: entity.stripeKey,
        ssn: entity.ssn,
        isActivated: entity.isActivated,
        lastModifiedBy: entity.lastModifiedBy,
        createdBy: entity.createdBy,
        deviceToken: entity.deviceToken,
        role: entity.role,
        token: entity.token,
        updatedOn: entity.updatedOn,
        createdOn: entity.createdOn,
    };
    return model;
};

exports.toSearchModel = entities => {
    return entities.map(entity => {
        return exports.toModel(entity);
    });
};
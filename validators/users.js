"use strict";
const response = require("../exchange/response");

const create = (req, res, next) => {
    const log = req.context.logger.start("validators:users:create");

    if (!req.body) {
        log.end();
        return response.failure(res, "body is equired");
    }
    if (!req.body.email) {
        log.end();
        return response.failure(res, "email is required");
    }
    if (!req.body.password) {
        log.end();
        return response.failure(res, "password is required");
    }
    if (!req.body.role) {
        log.end();
        return response.failure(res, "role is required (for super admin = SA ,for admin = A ,for user = U  )");
    }
    log.end();
    return next();
};



const login = (req, res, next) => {
    const log = req.context.logger.start("validators:users:login");

    if (!req.body) {
        log.end();
        return response.failure(res, "body is equired");
    }
    if (!req.body.email) {
        log.end();
        return response.failure(res, "email is required");
    }
    if (!req.body.password) {
        log.end();
        return response.failure(res, "password is required");
    }

    log.end();
    return next();
};

// change password
const changePassword = (req, res, next) => {
    const log = req.context.logger.start("validators:users:changePassword");

    if (!req.body.newPassword) {
        log.end();
        return response.failure(res, "password is required");
    }

    log.end();
    return next();
};

const update = (req, res, next) => {
    const log = req.context.logger.start("validators:users:update");

    if (!req.body) {
        log.end();
        return response.failure(res, "body is required");
    }
    if (!req.body._id) {
        log.end();
        return response.failure(res, "user id is required");
    }

    log.end();
    return next();
};

const addDetails = (req, res, next) => {
    const log = req.context.logger.start("validators:users:addDetails");

    if (!req.body) {
        log.end();
        return response.failure(res, "body is equired");
    }
    if (!req.body.name) {
        log.end();
        return response.failure(res, "name is required");
    }
    // if (!req.body.location) {
    //     log.end();
    //     return response.failure(res, "location is required");
    // }

    // if (!req.body.) {
    //   log.end();
    //   return response.failure(res, "ratio is required");
    // }


    log.end();
    return next();
};

const updateCustomer = (req, res, next) => {
    const log = req.context.logger.start("validators:users:updateCustomer");

    if (!req.body) {
        log.end();
        return response.failure(res, "body is required");
    }
    if (!req.body._id) {
        log.end();
        return response.failure(res, "user id is required");
    }

    log.end();
    return next();
};

const addOffers = (req, res, next) => {
    const log = req.context.logger.start("validators:users:addOffers");

    if (!req.body) {
        log.end();
        return response.failure(res, "body is equired");
    }
    if (!req.body.offers) {
        log.end();
        return response.failure(res, "offer is required");
    }

    log.end();
    return next();
};


exports.addDetails = addDetails;
exports.login = login;
exports.create = create;
exports.update = update;
exports.changePassword = changePassword;
exports.addOffers = addOffers;
exports.updateCustomer = updateCustomer;


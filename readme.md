Maninder Here's the FRS link where I have listed the screen by screen requirements. You can also download the Old App from Play store to understand the App. Client is expected to add new features/functionality for "Classes" and "Feed", Else it will be same as per the Android App available on store.

FRS Link:
https://docs.google.com/spreadsheets/d/1VrsXa1p40EGppDBu3b_YzQ5559yH1nl6xPncDBR-GQA/edit?usp=sharing

New Design Link with a few screens explaining "Classes" and "Feed":
https://www.figma.com/file/8CbufIFWf57VI4vW0ksaUD/HPS-APP-(1)?node-id=34%3A0

Android App link:
https://play.google.com/store/apps/details?id=com.housepointsystem.app&hl=en_IN

Let me know if you have any queries.

Thanks
Hi Vikram,
Here are the further details, If you have any query you can speak to Rumit as well.



"House Point:
https://github.com/digitalml/hps3_api
https://github.com/digitalml/hps3_android
https://github.com/digitalml/hps3_ios"
-------------------
autoincreameent
db.createCollection("counters");

-------------------
:: Pending List ::
- announcement

- add student to grades different api - done
- classes will have order and classes order can be updated same with periods, number of periods

- class periods list - done
- period students list
- student filter with classes and periods
- create parents invites
- reset student points
- reports
- newsfeed can have image as well.
- personal chat
- group chat
- add points to student by teacher
- substract points to student by teacher
- student login (only with school id)
- student dashboard
- student filter by house, by gradesof
- create different APIs for creating parent,Teacher




Vikram :
- Anouncements section :
There can be parents and teachers as well in the announcement section
- 





-------------------

===================
- in school detail
	- list of teachers
	- list of grades
- teacher filter
- student filter
- Super Admin Logout
- Super Admin ->  Add Houses to school
- 

- in school detail
	- list of teachers
	- list of grades
- teacher filter for search
- student filter for search
--------------------


--------------------

- student update 
- student delete 
- student list api
- Create class(Grade)
- Add students to Grade Class

--------------------

- create house- super admin - done
- update house - done
- delete house
- add points to house
- points history
- substract Points to house.


-----------
I have created 3 APIs today :

- add points to house
- substract points from house
- points history

------------------
- House List for super admin
- House Update
- House Delete
------------------------------


- Code review done by vikram and suggested him to improve.
- add students to house
- add points to multiple houses
- Review android screen videos and make changes in APis
-------------------
- remove grade
- add period/lecture // Assuming p1,p2,p3 on the screen are lectures
- update period/lecture
- delete period/lecture
- list period/lecture
- Code pushed over client repo


===================
Remove Grade:

api/grade/remove

Parameters :

id:5edc9a6c2301201bdb24ed1a

Response :

{
    "isSuccess": true,
    "statusCode": 200,
    "message": true
}


=============

- Created new API for Announcements
- Made changes in Login API role based login
  single Login API will be used for superamdin/Admin/Teacher/Parent
- Code pushed to client repo
- Google Sheet Updated


--------------
Login for all users :

/api/users/login

Super admin Login admin@housing.com / 123456

{
    "isSuccess": true,
    "statusCode": 200,
    "data": {
        "firstName": "Test User",
        "lastName": "Test User",
        "email": "Test User",
        "role": "SA",
        "deviceType": "android",
        "apiToken": "MVpZPD5APeWNx9hu94edWmfBveVDWVCT6Pq3Che3"
    },
    "message": "User Login Successfully"
}

-------------------------
For adding teacher :
{{base_url}}/api/users/add-teacher

-------------------
Base URL :

3.16.124.81:3000


For login teacher :
{{base_url}}/api/users/login
Parameters :
email:teacher@housing.com
password:12345
deviceType:android
deviceToken:dummyDeviceToken
role:T


NOTE ::
	super amdin ->  SA
	Admin/Teacher -> T
	Parent -> P
	Student ->  S // STUDENT HAVE NO LOGIN 


Repsone ::
{
    "isSuccess": true,
    "statusCode": 200,
    "data": {
        "firstName": "Teacher 1",
        "lastName": "Teacher 1",
        "email": "Teacher 1",
        "role": "T",
        "deviceType": "android",
        "apiToken": "B5D9Y4vpYBlQXzJVuPkJGpUQTBANRnCbibPfUsnU"
    },
    "message": "User Login Successfully"
}

/api/student/list

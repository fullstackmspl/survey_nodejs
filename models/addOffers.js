"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const addOffers = mongoose.Schema({
    id: { type: String, default: 0 },
    offers: { type: String, required: true },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
    isDeleted: { type: Boolean, default: false },

});
    
mongoose.model("addOffers", addOffers);
module.exports = addOffers;

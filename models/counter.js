"use strict";
const mongoose = require("mongoose");
const counter = mongoose.Schema({
  sequence_value: { type: Number, default: 0, required: true },  
  type: { type: String, default: "schools", required: true },  
});

mongoose.model("counter", counter);
module.exports = counter;
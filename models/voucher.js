"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const voucher = mongoose.Schema({
    
    email: { type: String },
    phoneNumber: { type: String},
    discount: { type: String },   
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
    isDeleted: { type: Boolean, default: false },

});

mongoose.model("voucher", voucher);
module.exports = voucher;

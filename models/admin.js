"use strict";
const mongoose = require("mongoose");
const admin = mongoose.Schema({
  firstName: { type: String, default: "", required: true },
  lastName: { type: String, default: "", required: true },
  sex: { type: String, default: "" },
  email: { type: String, default: "", required: true },
  status: {
    type: String, default: "active",
    enum: ["active", "inActive"]
  },
  role: { type: String, default: "" },
  password: { type: String, default: "" },
  deviceToken: { type: String, default: "" },
  deviceType: { type: String, default: "android" },
  apiToken: { type: String, default: "" },
  createdOn: { type: Date, default: Date.now },
  updatedOn: { type: Date, default: Date.now }
});

mongoose.model("admin", admin);
module.exports = admin;
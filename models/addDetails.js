"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const addDetails = mongoose.Schema({
    name: { type: String, required: true },
    id: { type: String, default: 1 },
    email: { type: String },
    flat: { type: String },
    city: { type: String },
    street: { type: String },
    state: { type: String },
    lat: { type: Number },
    lng: { type: Number},
    contactNumber: { type: String },
    gasType: { type: String },
    loyalityNumber: { type: String },
    developmentArea: { type: String },
    community: { type: String },
    District: { type: String },
    offers: { type: String },
    location: { type: String },
    notes: { type: String  },
    parish: { type: String  },
    purchaseOn: { type: String },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});

mongoose.model("addDetails", addDetails);
module.exports = addDetails;

"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const addGasType = mongoose.Schema({
    id: { type: String, default: 0 },
    gasType: { type: String, required: true },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
    isDeleted: { type: Boolean, default: false },

});
    
mongoose.model("addGasType", addGasType);
module.exports = addGasType;

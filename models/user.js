"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const user = mongoose.Schema({
    name: { type: String },
    phoneNumber: { type: String },
    email: { type: String, default: "" },
    role: {
        type: String,
    },

    resetPasswordToken: {
        type: String,
        required: false
    },

    resetPasswordExpires: {
        type: Date,
        required: false
    },
    address: { type: String, default: "" },
    city: { type: String, default: "" },
    country: { type: String, default: "" },
    zipCode: { type: String, default: "" },
    zipCode: { type: String, default: "" },
    zipCode: { type: String, default: "" },
    password: { type: String, default: "" },
    apiToken: { type: String, default: "" },
    personalNote: { type: String, default: "" },
    deviceToken: { type: String, default: "" },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
    isDeleted: { type: Boolean, default: false },
    id: { type: Number, default: 0 },
    lat: { type: Number, default: "" },
    lng: { type: Number, default: "" },

});

mongoose.model("user", user);
module.exports = user;
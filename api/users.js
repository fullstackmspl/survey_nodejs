"use strict";
const service = require("../services/userService");
const response = require("../exchange/response");
const encrypt = require("../permit/crypto.js");
const userMapper = require("../mappers/user");

//login api  

const login = async (req, res) => {
    const log = req.context.logger.start("api:users:login");
    try {
        const user = await service.login(req.body, req.context);


        log.end();
        return response.authorized(res, user, user.token);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//register api

const create = async (req, res) => {
    const log = req.context.logger.start(`api:users:create`);

    try {
        const user = await service.create(req.body, req.context);
        const message = "User Register Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// addetails

const addDetails = async (req, res) => {
    const log = req.context.logger.start(`api:users:addDetails`);

    try {
        const user = await service.addDetails(req.body, req.context);
        const message = "Customer added Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const updateCustomer = async (req, res) => {
    const log = req.context.logger.start(`api:users:updateCustomer`);
    try {
        const user = await service.updateCustomer(req.body, req.context);
        log.end();
        return response.data(res, userMapper.toModel(user));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//getDetails
const getDetails = async (req, res) => {
    const log = req.context.logger.start(`api:users:getDetails`);
    try {
        const user = await service.getDetails(req.body, req.context);
        const message = "Customers get Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// add offers

const addOffers = async (req, res) => {
        const log = req.context.logger.start(`api:users:addOffer`);

        try {
            const users = await service.addOffers(req.body, req.context);
            const message = "Offers added Successfully";
            log.end();
            return response.success(res, message, users);
        } catch (err) {
            log.error(err);
            log.end();
            return response.failure(res, err.message);
        }
    };

// add offers

const addGasType = async (req, res) => {
    const log = req.context.logger.start(`api:users:addGasType`);

    try {
        const users = await service.addGasType(req.body, req.context);
        const message = "gasType added Successfully";
        log.end();
        return response.success(res, message, users);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// add location

const voucher = async (req, res) => {
    const log = req.context.logger.start(`api:users:voucher`);

    try {
        const users = await service.voucher(req.body, req.context);
        const message = "voucher added Successfully";
        log.end();
        return response.success(res, message, users);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// get Location

// const getLocation = async (req, res) => {
//     const log = req.context.logger.start(`api:users:getLocation`);

//     try {
//         const users = await service.getLocation(req.body, req.context);
//         const message = "location get Successfully";
//         log.end();
//         return response.success(res, message, users);
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }
// };

// current user
const currentUser = async (req, res) => {
    const log = req.context.logger.start(`api:users:currentUser`);
    try {
        const user = await service.currentUser(req.body, req.context);
        const message = "Current User";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// change password

const changePassword = async (req, res) => {
    const log = req.context.logger.start("api:users:changePassword");
    try {
        const message = await service.changePassword(req.body, req.context);
        log.end();
        return response.success(res, message);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// FORGET PASSWORD

const voucherMail = async (req, res) => {
    const log = req.context.logger.start(`api:users:voucherMail`);
    try {
        const message = await service.voucherMail(req.body, req.context);
        log.end();
        return response.success(res, message);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//getUsers
const getUsers = async (req, res) => {
    const log = req.context.logger.start(`api:users:getUsers`);
    try {
        const user = await service.getUsers(req.body, req.context);
        const message = "User get Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

//getOffers
const getOffers = async (req, res) => {
    const log = req.context.logger.start(`api:users:getOffers`);
    try {
        const user = await service.getOffers(req.body, req.context);
        const message = "Offers get Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const deleteUser = async (req, res) => {
    const log = req.context.logger.start(`api:users:deleteUser`);
    try {
        const user = await service.deleteUser(req.body, req.context);
        log.end();
        return response.data(res, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async (req, res) => {
    const log = req.context.logger.start(`api:users:update`);
    try {
        const user = await service.update(req.body, req.context);
        log.end();
        return response.data(res, userMapper.toModel(user));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const uploadProfilePic = async (req, res) => {
    const log = req.context.logger.start(`api:users:uploadProfilePic`);
    try {
        const user = await service.uploadProfilePic(req.params.id, req.file, req.context);
        const message = "Profile Picture Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }

};


//getOffers
const getGasType = async (req, res) => {
    const log = req.context.logger.start(`api:users:getGasType`);
    try {
        const user = await service.getGasType(req.body, req.context);
        const message = "Offers get Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const deleteGasType = async (req, res) => {
    const log = req.context.logger.start(`api:users:deleteGasType`);
    try {
        const user = await service.deleteGasType(req.body, req.context);
        log.end();
        return response.data(res, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const deleteOffers = async (req, res) => {
    const log = req.context.logger.start(`api:users:deleteOffers`);
    try {
        const user = await service.deleteOffers(req.body, req.context);
        log.end();
        return response.data(res, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

// usersByDate

// const usersByDate = async (req, res) => {
//     const log = req.context.logger.start(`api:users:usersByDate`);
//     try {
//         const users = await service.getUsersByDate(req.body, req.context);
//         console.log("sjdj",users)
//         log.end();
//         return response.data(res, users);
//     } catch (err) {
//         log.error(err);
//         log.end();
//         return response.failure(res, err.message);
//     }
// };

const tellAFriend = async (req, res) => {
    const log = req.context.logger.start(`api:users:tellAFriend`);
    try {
      const user = await service.tellAFriend(req.body, req.context);
      const message = "Message sent Successfully";
      log.end();
      return response.success(res, message);
    } catch (err) {
      log.error(err);
      log.end();
      return response.failure(res, err.message);
    }
  };


  const sendMessage = async (req, res) => {
    const log = req.context.logger.start("api:users:sendOtp");
    try {
      const data = await service.sendMessage(req.body, req.context);
      log.end();
      return response.success(res, data);
    } catch (err) {
      log.error(err);
      log.end();
      return response.failure(res, err.message);
    }
  };


  const getProvidersByDate = async (req, res) => {
    const log = req.context.logger.start(`api:users:getProvidersByDate`);
    try {
        const providers = await service.getProvidersByDate(req.query, req.context);
        log.end();
        return response.data(res, providers);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};
exports.login = login;
exports.create = create;
exports.currentUser = currentUser;
exports.changePassword = changePassword;
exports.addDetails = addDetails;
exports.voucherMail = voucherMail;
exports.getUsers = getUsers;
exports.deleteUser = deleteUser;
exports.update = update;
exports.uploadProfilePic = uploadProfilePic;
exports.addOffers = addOffers;
exports.getOffers = getOffers;
exports.voucher = voucher;
exports.getDetails = getDetails;
exports.updateCustomer = updateCustomer;
exports.addGasType = addGasType;
exports.getGasType = getGasType;
exports.deleteGasType = deleteGasType;
exports.deleteOffers = deleteOffers;
// exports.usersByDate = usersByDate;
exports.tellAFriend = tellAFriend;
exports.sendMessage = sendMessage;
exports.getProvidersByDate = getProvidersByDate;







// exports.addDetail = addDetail;
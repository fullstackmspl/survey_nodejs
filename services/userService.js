const encrypt = require("../permit/crypto.js");
const auth = require("../permit/auth");
const path = require("path");
const { compareSync } = require("bcrypt");
const imageUrl = require('config').get('image').url
const ObjectId = require("mongodb").ObjectID;
var nodemailer = require('nodemailer')
const crypto = require("crypto");
const moment = require('moment');
const accountSid = 'ACc01eb200b2a135353782ab09c0599e66';
const authToken = '84ca540672d8a11404af6b66e7e7f098';
const client = require('twilio')(accountSid, authToken);



const setUser = (model, user, context) => {
  const log = context.logger.start("services:users:set");
  if (model.name !== "string" && model.name !== undefined) {
    user.name = model.name;
  }

  if (model.phoneNumber !== "string" && model.phoneNumber !== undefined) {
    user.phoneNumber = model.phoneNumber;
  }

  if (model.role !== "string" && model.role !== undefined) {
    user.role = model.role;
  }

  if (model.sex !== "string" && model.sex !== undefined) {
    user.sex = model.sex;
  }
  if (model.address !== "string" && model.address !== undefined) {
    user.address = model.address;
  }
  if (model.city !== "string" && model.city !== undefined) {
    user.city = model.city;
  }
  if (model.country !== "string" && model.country !== undefined) {
    user.country = model.country;
  }
  if (model.lat !== "string" && model.lat !== undefined) {
    user.lat = model.lat;
  }
  if (model.lng !== "string" && model.lng !== undefined) {
    user.lng = model.lng;
  }
  if (model.zipCode !== "string" && model.zipCode !== undefined) {
    user.zipCode = model.zipCode;
  }

  log.end();
  user.save();
  return user;

};
//register user

const buildUser = async (model, context) => {
  const { name, phoneNumber, email, password, role, address, city, lat, lng, country, zipCode } = model;
  const log = context.logger.start(`services:users:build${model}`);
  const user = await new db.user({
    name: name,
    phoneNumber: phoneNumber,
    email: email,
    role: role,
    address: address,
    city: city,
    lat: lat,
    lng: lng,
    country: country,
    zipCode: zipCode,
    password: password,
    createdOn: new Date(),
    updateOn: new Date()
  }).save();
  log.end();
  return user;
};

const create = async (model, context) => {
  const log = context.logger.start("services:users:create");
  const isEmail = await db.user.findOne({ email: { $eq: model.email } });

  if (isEmail) {
    //return "Email already resgister";
    throw new Error("Email already exists");
  } else {
    model.password = encrypt.getHash(model.password, context);
    const user = buildUser(model, context);
    log.end();
    return user;
  }

};

// login 

const login = async (model, context) => {
  const log = context.logger.start("services:users:login");

  const query = {};

  if (model.email) {
    query.email = model.email;
  }
  console.log('parameter', query)

  let user = await db.user.findOne(query)
  console.log('jhfjd', user)

  if (!user) {
    log.end();
    throw new Error("user not found");
  }
  const isMatched = encrypt.compareHash(model.password, user.password, context);

  if (!isMatched) {
    log.end();
    throw new Error("password mismatch");
  }

  const token = auth.getToken(user.id, false, context);
  user.apiToken = token;
  user.updatedOn = new Date();
  user.save();
  log.end();
  return user;
};

// createOne

const addDetails = async (model, context) => {
  const { name, email, flat, street, notes, state, city, contactNumber, gasType, loyalityNumber, offers, location, community, lat, lng, district, developmentArea, purchaseOn, parish } = model;
  const log = context.logger.start(`services:users:build${model}`);
  const user = await new db.addDetails({
    name: name,
    email: email,
    flat: flat,
    street: street,
    city: city,
    state: state,
    contactNumber: contactNumber,
    gasType: gasType,
    loyalityNumber: loyalityNumber,
    offers: offers,
    location: location,
    community: community,
    district: district,
    lat: lat,
    lng: lng,
    developmentArea: developmentArea,
    notes: notes,
    parish: parish,
    purchaseOn: purchaseOn,

  }).save();
  log.end();
  return addDetails;
};

// updateCustomer
const updateCustomer = async (model, context) => {
  const log = context.logger.start(`services:users:updateCustomer`);
  let entity = await db.addDetails.findOne({ _id: { $eq: model._id } });
  if (!entity) {
    throw new Error("invalid user");
  }
  const user = await setUser(model, entity, context);
  log.end();
  return user
};

const sendMessage = async (model, context) => {
  const log = context.logger.start('services/users/sendMessage')
  const newNumbers = model.phoneNumber
  const whatsappMessage = model.whatsappMessage
  console.log("hello", newNumbers)
  client.messages
    .create({
      from: 'whatsapp:+14155238886',
      body: whatsappMessage,
      to: newNumbers

    })
    .then(message => console.log(message.sid));
}


const tellAFriend = async (model, context) => {
  const log = context.logger.start('services/users/tellAFriend')
  if (!model.email) {
    throw new Error("email not found");
  }

  let message = `<style>
.blue_button_wraper {
  background: #043d99;
  color: #ffffff;
  height: 80px;
  display: flex;
  align-items: center;
  justify-content: center;
}

.blue_button {
  font-size: 20px;
  font-weight: 600;
}

.logo_img {
  text-align: center;
}

.thank_text {
  padding-top: 15px;
  text-align: center;
}

hr.dotted {
  border-style: dashed;
}

th.header_line {
  border-bottom: 2px solid #000000 !important;
  border-top: none;
}

.red_button_wraper {
  background: #b41127;
  height: 80px;
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
}

.red_button {
  font-size: 20px;
  font-weight: 600;
}

body.my_custom_page .col-4 {
  padding-right: 0px;
}

body.my_custom_page .col-8 {
  padding-left: 0;
}

hr.double {
  margin-top: 2px;
  margin-bottom: 4px;
  border-top: 2px solid rgb(0 0 0);
}

.queries {
  text-align: center;
  font-weight: 500;
}
</style>
</head>

<body class="my_custom_page">
<div class="container">
<div class="header">
  <div class="row">
    <div class="col-4">
      <div class="logo_img">
        <img src="logocwh.png">
      </div>
    </div>
    <div class="col-8">
      <div class="blue_button_wraper">
        <div class="blue_button">
          OFFER VOUCHER
        </div>
      </div>
    </div>
  </div>
</div>
<div class="thank_text">
  THANK YOU FOR USING OUR SERVICES
</div>
<hr class="dotted">
<br>
<br>

<table class="table table-striped">
  <tbody>
    <tr>
      <td>Voucher ID : 1</td>

    </tr>
    <tr>
      <td>Customer Name </td>

    </tr>
    <tr>
      <td>Customer Loyalty No.1</td>

    </tr>
    <tr>
      <td>Mobile No.</td>
    </tr>
  </tbody>
</table>

<br>
<br>

<table class="table table-striped">
  <thead>
    <tr>
      <th class="header_line">Address</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>xxxxxxx</td>
    </tr>
    <tr>
      <td>xxxxxxxx</td>
    </tr>
    <tr>
      <td>xxxxxx </td>
    </tr>
  </tbody>
</table>
<div class="last_section">
  <div class="row">
    <div class="col-4">
      <div class="red_button_wraper">
        <div class="red_button">
          OFFER AMOUNT
        </div>
      </div>
    </div>
    <div class="col-8">
      <div class="blue_button_wraper">
        <div class="blue_button">
        ${model.personalNote}
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-4">
    For Product
  </div>
  <div class="col-8">
    IGL 14
  </div>
</div>

<hr class="double">
<hr class="double">
<div class="queries">
  For any queries contact: 876-371-2665 or 876-314-26665
</div>
</div>

</body>
`

  //old template// 


  //     let message = `hi ${model.fullName}, <br>  you will get discount of  <br> ${model.personalNote} 
  //     <div class="container">
  //     <div class="text-center">
  //     <h5 class="font-weight-bold">Sample E‐mail Request to Participate in Internet Survey</h5>
  // </div>
  // <div class="row">
  //     <div class="col-md-3">
  //        <label> 
  //            Sender Information 
  //     </label>
  //     </div>
  //     <div class="col-md-9">
  //       From:	Robert	Merton Sent: Monday,November 29,2010 8:30 p.m.	
  //             To: bobstudent@wpi.edu
  //     </div>
  // </div>

  // <div class="row">
  //     <div class="col-md-3">
  //        <label> 
  //         Informative	Subject
  //     </label>
  //             </div>
  //             <div class="col-md-9">
  //                 Subject:	Survey	of	Students	Who	Took	Methods	of	Social	
  //                 Research	      
  //             </div>
  //     </div> 

  //     <div class="row">
  //         <div class="col-md-3">
  //            <label> 
  //             Appeal	for	help

  //           <p>  Why	you	were	
  // selected	and	what	
  // it’s	about	
  // </p>
  // <p>  Usefulness	of	survey

  // </p>
  //         </label>
  //                 </div>
  //                 <div class="col-md-9">
  //                     I	am	writing	to	you	to	request	your	participation	in	a	brief	
  //                     survey.	As	you	may	recall	from	Sociology	224,	Methods	of	
  //                     Social	Research,	your	class	along	with	students	from	other	
  //                     liberal	arts	colleges	in	California	and	Maine	participated	in	a
  //                     multi‐college	survey.	The	instructors	would	like	to	get	more	
  //                     feedback	about	your	experiences	with	this	project.	Your	
  //                     responses	to	this	survey	will	help	us	evaluate	the	effectiveness	
  //                     of	the	multi‐college	survey	so	that	we	can	design	better	
  //                     projects	and	improve	the	teaching	of	research	methods.		      
  //                 </div>
  //         </div> 

  //         <div class="row">
  //             <div class="col-md-3">
  //                <label> 
  //                 Clickable	link	
  //             </label>
  //                     </div>
  //                     <div class="col-md-9">
  //                         Survey	link:	http://www.surveymonkey.com/s/XXXXXXX		      
  //                     </div>
  //             </div> 

  //             <div class="row">
  //                 <div class="col-md-3">
  //                    <label> 
  //                     Individualized	ID
  //                 </label>
  //                         </div>
  //                         <div class="col-md-9">
  //                             Personal	Access	Code:	4111944		      
  //                         </div>
  //                 </div> 
  //                 <div class="row">
  //                     <div class="col-md-3">
  //                        <label> 
  //                         Confidentia	and	voluntary	
  //                     </label>
  //                             </div>
  //                             <div class="col-md-9">
  //                                 Your	participation	in	the	survey	is	completely	voluntary	and	all	
  // of	your	responses	will	be	kept	confidential.	The	access	code	is
  // to	remove	you	from	the	list	once	you	have	completed	the	
  // survey.	No	personally	identifiable	information	will	be	
  // associated	with	your	responses	to	any	reports	of	these	data.	
  // The	WPI	Institutional	Review	Board	has	approved	this	survey.	
  // Should	you	have	any	comments	or	questions,	please	feel	free	to	
  // contact	me	at	rmerton@wpi.edu	or	444‐444‐4444.	      
  //                             </div>
  //                     </div> 
  //                     <div class="row">
  //                         <div class="col-md-3">
  //                            <label> 
  //                             Thank	you	
  //                             Importance	
  //                         </label>
  //                                 </div>
  //                                 <div class="col-md-9">
  //                                     Thank	you	very	much	for	your	time	and	cooperation.	Feedback	
  // from	our	students	is	very	important	to	us.	 

  // Sincerely,	
  // Robert	Merton	
  // Professor	of	Sociology	
  // WPI	
  //                                 </div>
  //                         </div>

  // <div>
  // </div>
  // </div>
  //     `
  subject = 'invitation'
  await sendMail(model.email, message, subject)
  log.end()
  return
}

const sendMail = async (email, message, subject) => {
  var smtpTrans = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: `javascript.mspl@gmail.com`,
      pass: `showmydev#$!45`
    }
  });
  // email send to registered email
  var mailOptions = {
    from: 'survey',
    to: email,
    subject: subject,
    html: message
  };

  let mailSent = await smtpTrans.sendMail(mailOptions)
  if (mailSent) {
    console.log("Message sent: %s", mailSent.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(mailSent));
    return
  } else {
    log.end()
    throw new Error("Unable to send email try after sometime");
  }
}

// getDetails
const getDetails = async (query, context) => {
  const log = context.logger.start(`services:users:getDetails`);
  let getOffers = await db.addDetails.find({ $and: [{ id: '1' }] });
  getOffers.count = await db.addDetails.find({ $and: [{ id: '1' }] }).count();
  log.end();
  return getOffers;
};

// const  getDetaildDate = async (query, context) =>
// {
//     const log = context.logger.start(`services:users:getDetails`);
//     await db.addDetails.aggregate([
//         {
//             $match: { "createdOn": { $gte: new Date(query.fromDate), $lt: new Date(query.toDate) } }
//         },])
//         if(fromDate < createdOn && toDate > createdOn){

//         }
// }

// const getUsersByDate = async (model, context) => {
//   const log = context.logger.start(`services:userService:getUsersByDate`);
//   let query = {}
//   query["createdOn"] = { $gte: model.fromDate }
//   query["createdOn"] = { $lte: model.toDate }
//   let users = await db.addDetails.find(query)
//   if (!users.length) {
//     throw new Error("Users Not found");
//   }
//   return users;


// };

// const getUsersByDate = async (query, context) => {
//   const { fromDate, toDate } = query;
// console.log("from date is ",fromDate)
//   const log = context.logger.start(`services:userService:getUsersByDate`);
//   const dat = {
//     '$gte': moment(fromDate, "DD-MM-YYYY").startOf('day').toDate(),
//     '$lt': moment(toDate, "DD-MM-YYYY").endOf('day').toDate()
//   }
//   let users = await db.addDetails.find({ createdOn: dat });
//   log.end();
//   console.log("hello",users)
//   return users;
// };

// add offers

const addOffers = async (model, context) => {
  const { offers } = model;
  const log = context.logger.start(`services:users:build${model}`);
  const user = await new db.addOffers({
    offers: offers,
  }).save();
  log.end();
  return addOffers;
};

// add gasType

const addGasType = async (model, context) => {
  const { gasType } = model;
  const log = context.logger.start(`services:users:build${model}`);
  const user = await new db.addGasType({
    gasType: gasType,
  }).save();
  log.end();
  return addGasType;
};

// add location

const voucher = async (model, context) => {
  const { email, phoneNumber, discount } = model;
  const log = context.logger.start(`services:users:build${model}`);
  const user = await new db.voucher({
    email: email,
    phoneNumber: phoneNumber,
    discount: discount
  }).save();
  log.end();
  return voucher;
};

// getLocation
// const getLocation = async (model, context) => {
//     const log = context.logger.start(`services:users:getLocation`);
//     let entity = await db.voucher.findOne({ _id: { $eq: model._id } });
//     if (!entity) {
//         throw new Error("invalid user");
//     }
//     const user = await setUser(model, entity, context);
//     log.end();
//     return user
// };


// change password

const changePassword = async (model, context) => {
  const log = context.logger.start(`service/users/changePassword`);
  if (!model._id) {
    log.end();
    throw new Error("user id is required");
  }
  let user = await db.user.findOne({ _id: { $eq: model._id } });
  if (!user) {
    log.end();
    throw new Error("user is not found");
  }
  const isMatched = encrypt.compareHash(
    model.oldPassword,
    user.password,
    context
  );
  if (isMatched) {
    const newPassword = encrypt.getHash(model.newPassword, context);
    user.password = newPassword;
    user.updatedOn = new Date();
    await user.save();
    log.end();
    return "Password Updated Successfully";
  } else {
    log.end();
    throw new Error("Old Password Not Match");
  }
};

// forget_password



const voucherMail = async (model, context) => {
  const log = context.logger.start('service/users/voucherMail');
  const reset = {};
  const resetPasswordToken = crypto.randomBytes(20).toString("hex");
  const resetPasswordExpires = Date.now() + 3600000; //expires in an hour

  reset.resetPasswordToken = resetPasswordToken;
  reset.resetPasswordExpires = resetPasswordExpires;

  async function storedToken() {
    await db.addDetails.findOneAndUpdate({ email: model.email }, { $set: reset }, { new: true }).then((tkn) => {
      console.log("reset password token is stored in db", tkn);
    });
  }

  let user = await db.addDetails.findOne({ email: model.email })

  if (!user) {
    log.end();
    throw new Error("The email address " + model.email + " does not exist with any account. Please check your email address and try again.");
  } else {
    storedToken();

    let smtpTransport = nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      auth: {
        user: 'harsimrans483@gmail.com',
        pass: 'Simar@123'
      }
    });

    if (user) {
      // const link = 'http://' + req.headers.host + '/reset/' + resetPasswordToken;
      const link = 'http://localhost:8800/voucherMail/' + resetPasswordToken;

      let mailOptions = {
        from: "smtp.mailtrap.io",
        to: `${user.email}`,
        subject: "Link To The Reset Password",
        html: "Hi" +
          "<b style='text-transform: capitalize;'>" + `${user.userName}` + "</b>" +
          "<br>  Please Click on the following link to verify your email to reset password. This is valid for one hour of receiving it.<br><a href=" +
          link +
          ">Click here to verify</a> <br> 'If you did not request this, please ignore this email and your password will remain unchanged.'",
      };
      smtpTransport.sendMail(mailOptions, function (error, info) {
        if (!error) {
          log.end();
          return "Recovery email sent"
        } else {
          log.end();
          return error.message
        }
      });
    }
  }

};

// getUsers
const getUsers = async (query, context) => {
  const log = context.logger.start(`services:users:getUsers`);
  let allUsers = await db.user.find({ $and: [{ role: 'U' }, { isDeleted: false }] });
  allUsers.count = await db.user.find({ $and: [{ role: 'U' }, { isDeleted: false }] }).count();
  log.end();
  return allUsers;
};

// getUsers
const getOffers = async (query, context) => {
  const log = context.logger.start(`services:users:getOffers`);
  let allOffers = await db.addOffers.find({ $and: [{ id: '0' }, { isDeleted: false }] });
  allOffers.count = await db.addOffers.find({ $and: [{ id: '0' }, { isDeleted: false }] }).count();
  log.end();
  return allOffers;
};

const currentUser = async (model, context) => {
  const log = context.logger.start(`services:users:currentUser`);
  let user = await db.user.findOne({ _id: { $eq: model.id } });
  log.end();
  return user;
};

const deleteUser = async (model, context) => {
  const log = context.logger.start(`services:users:deleteUser`);
  if (!model.id) {
    throw new Error("userId is requried");
  }
  let user = await db.user.deleteOne({ _id: { $eq: model.id } });
  if (!user) {
    throw new Error("user not found");
  }
  user.message = 'User Deleted Successfully'
  return user
};

const update = async (model, context) => {
  const log = context.logger.start(`services:users:update`);
  let entity = await db.user.findOne({ _id: { $eq: model._id } });
  if (!entity) {
    throw new Error("invalid user");
  }
  const user = await setUser(model, entity, context);
  log.end();
  return user
};


const uploadProfilePic = async (id, file, context) => {
  const log = context.logger.start(`services:users:uploadProfilePic`);
  let user = await db.user.findById(id);
  if (!file) {
    throw new Error("image not found");
  }
  if (!user) {
    throw new Error("user not found");
  }
  if (user.avatar != "" && user.avatar !== undefined) {

    let picUrl = user.avatar.replace(`${imageUrl}`, '');
    try {
      await fs.unlinkSync(`${picUrl}`)
      console.log('File unlinked!');
    } catch (err) {
      console.log(err)
    }
  }
  const avatar = imageUrl + 'assets/images/' + file.filename
  user.avatar = avatar
  await user.save();
  log.end();
  return user
};

// getGasType
const getGasType = async (query, context) => {
  const log = context.logger.start(`services:users:getGasType`);
  let gasType = await db.addGasType.find({ $and: [{ id: '0' }] });
  gasType.count = await db.addGasType.find({ $and: [{ id: '0' }] }).count();
  log.end();
  return gasType;
};


const deleteGasType = async (model, context) => {
  const log = context.logger.start(`services:users:deleteGasType`);
  if (!model.id) {
    throw new Error("userId is requried");
  }
  let user = await db.addGasType.deleteOne({ _id: { $eq: model.id } });
  if (!user) {
    throw new Error("user not found");
  }
  user.message = 'User Deleted Successfully'
  return user
};

const deleteOffers = async (model, context) => {
  const log = context.logger.start(`services:users:deleteOffers`);
  if (!model.id) {
    throw new Error("userId is requried");
  }
  let user = await db.addOffers.deleteOne({ _id: { $eq: model.id } });
  if (!user) {
    throw new Error("user not found");
  }
  user.message = 'User Deleted Successfully'
  return user
};



const getProvidersByDate = async (query, context) => {
  const { fromDate, toDate } = query;

  const log = context.logger.start(`services:users:getProvidersByDate`);
  const dat = {
    '$gte': moment(fromDate, "DD-MM-YYYY").startOf('day').toDate(),
    '$lt': moment(toDate, "DD-MM-YYYY").endOf('day').toDate()
  }
  let providers = await db.addDetails.find({ createdOn: dat });
  log.end();
  return providers;
};


exports.login = login;
exports.create = create;
exports.currentUser = currentUser;
exports.changePassword = changePassword;
exports.addDetails = addDetails;
exports.voucherMail = voucherMail;
exports.getUsers = getUsers;
exports.deleteUser = deleteUser;
exports.update = update;
exports.uploadProfilePic = uploadProfilePic;
exports.addOffers = addOffers;
exports.getOffers = getOffers;
exports.voucher = voucher;
// exports.getLocation = getLocation;
exports.getDetails = getDetails;
exports.updateCustomer = updateCustomer;
exports.addGasType = addGasType;
exports.getGasType = getGasType;
exports.deleteGasType = deleteGasType;
exports.deleteOffers = deleteOffers;
// exports.getUsersByDate = getUsersByDate;
exports.tellAFriend = tellAFriend;
exports.sendMessage = sendMessage;
exports.getProvidersByDate = getProvidersByDate;







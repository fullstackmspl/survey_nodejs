var multer = require('multer');
var baseDir = process.cwd();
var uploads = multer({
	dest: baseDir + "/uploads/"
});



var fs = require('fs');
//const Sequelize = require('sequelize'); // seqelize lib

var uploadPathw = baseDir + "/uploads/";
var imgExt = {
	'image/png': '.png',
	'image/jpeg': '.jpeg',
	'image/jpg': '.jpg',
	'text/csv': '.csv',
	
};
var Promise = require('promise');
var monthNames = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

const uploadFile = async (req, res, uploadFolder,type) => {
	console.log("=====================")
	//function uploadFile(req, res, uploadFolder) {
		// app.post('/profile', upload.single('avatar'), function (req, res, next) {
		//   // req.file is the `avatar` file
		//   // req.body will hold the text fields, if there were any
		// })
		//console.log(uploadFolder)
		//var reqfile = uploads.single(type)
		//console.log("req.file",req.file);
		//console.log(req);
		//return new Promise(function (resolve, reject) {
			var tmp_path = req.path;
			console.log("tmp_path",tmp_path);
			var uploadPath = uploadPathw + uploadFolder;
			console.log("uploadPath : "+uploadPath);
			
			if (!fs.existsSync(uploadPath)) {
				fs.mkdirSync(uploadPath);
			}
			console.log("req.file.mimetype",req.file.mimetype);
			if (imgExt[req.file.mimetype] !== undefined) {
				var fileName = req.file.filename + imgExt[req.file.mimetype];
				var target_path = uploadPath + '/' + fileName;
				/** A better way to copy the uploaded file. **/
				var src = fs.createReadStream(tmp_path);
				var dest = await fs.createWriteStream(target_path);
				unlinkFile(tmp_path);
				src.pipe(dest);
				src.on('end', function () {
					return {
						status: 200,
						response: 'Uploaded Successfully!',
						name: fileName
					}
					// resolve({
					// 	status: 200,
					// 	response: 'Uploaded Successfully!',
					// 	name: fileName
					// });
				});
				src.on('error', function (err) {
					// reject({
					// 	status: 501,
					// 	response: err
					// });
					return {
						status: 501,
						response: err
					}
				});
			} else {
				return {
					status: 501,
					response: 'This file is not allowed'
				};
				// reject({
				// 	status: 501,
				// 	response: 'This file is not allowed'
				// });
			}
		//});
	}

exports.uploadFile = uploadFile;